package com.projecta.tomcatconf;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TomcatConfReplace {
    public static void main(String[] args) {
        if (args.length > 1 && !args[0].isEmpty() && !args[1].isEmpty()) {
            TomcatConfReplace app = new TomcatConfReplace();
            app.replace(args[0], args[1]);
        } else {
            System.out.println("Incorrect number of arguments");
        }
    }

    public void replace(String CONF_FILE, String REPLACEMENT) {
        File source = null;
        File backup = null;
        File target = null;
        BufferedWriter writer = null;

        try {
            // Create File objects
            source = new File(CONF_FILE);
            backup = new File(CONF_FILE + ".bak");

            // Rename target (backup)
            if (!source.renameTo(backup)) {
                throw new IOException("Couldn't rename source to backup");
            }

            // Create target
            target = new File(CONF_FILE);

            // Create temp file and writer
            writer = new BufferedWriter(new FileWriter(target));

            // Get contents of source
            List<String> lines = Files.readAllLines(
                    Paths.get(backup.getPath()), Charset.defaultCharset());

            // Loop through each line in contents, and append replaced lines to
            // tmp file
            for (final String line : lines) {
                Pattern pattern = Pattern
                        .compile("wrapper\\.java\\.additional\\.[0-9]+=-Xm[x|s]([0-9]+[a-zA-Z]+)");
                Matcher matcher = pattern.matcher(line);
                String finalLine;

                if (matcher.matches())
                    finalLine = (line.replaceAll(matcher.group(1), REPLACEMENT));
                else
                    finalLine = line;

                writer.append(finalLine);
                writer.newLine();
            }
            writer.flush();

            // Delete backup
            backup.deleteOnExit();

        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            if (target != null)
                target.deleteOnExit();
            if (backup != null && source != null)
                backup.renameTo(source);
            return;
        } finally {
            if (writer != null)
                try {
                    writer.close();
                } catch (IOException e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
        }
    }
}
